package BL;

import BL.MessageInputListener;

public interface MessageInputService {
    void initialize(MessageInputListener messageInputListener);
    void shutdown();
}
