package BL;
import com.rabbitmq.client.*;

import java.io.IOException;
import java.net.URISyntaxException;
import java.security.KeyManagementException;
import java.security.NoSuchAlgorithmException;

public class RabbitMQ implements MessageInputService {

    private final String connectionString;
    private final String queueName;
    private final MessageFormatter formatter;

    private Connection connection;
    private Channel channel;


    public RabbitMQ(String connectionString, String queueName, MessageFormatter formatter) {
        this.connectionString = connectionString;
        this.queueName = queueName;
        this.formatter = formatter;
    }
    @Override
    public void initialize(MessageInputListener messageInputListener) {
        ConnectionFactory factory = new ConnectionFactory();
        try {
            factory.setUri(connectionString);
        } catch (URISyntaxException e) {
            e.printStackTrace();
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        } catch (KeyManagementException e) {
            e.printStackTrace();
        }

        //Recommended settings
        factory.setRequestedHeartbeat(30);
        factory.setConnectionTimeout(30000);

        try {
            connection = factory.newConnection();
        } catch (IOException e) {
            e.printStackTrace();
        }
        try {
            channel = connection.createChannel();
        } catch (IOException e) {
            e.printStackTrace();
        }

        try {
            channel.queueDeclare(queueName,
                    false, /* non-durable */
                    false, /* non-exclusive */
                    false, /* do not auto delete */
                    null); /* no other construction arguments */
        } catch (IOException e) {
            e.printStackTrace();
        }


        Consumer consumer = new DefaultConsumer(channel) {
            @Override
            public void handleDelivery(String consumerTag, Envelope envelope, AMQP.BasicProperties properties, byte[] body)
                    throws IOException {
                String content = new String(body, "UTF-8");
                System.out.println("Receiving object: " + content);
                messageInputListener.receive(formatter.formatMessage(content));

            }
        };
        try {
            channel.basicConsume(queueName, true, consumer);

        } catch (IOException e) {
            e.printStackTrace();
        }

    }

    @Override
    public void shutdown() {
        try {
            channel.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
        try {
            connection.close();
        } catch (IOException e) {
            e.printStackTrace();
        }

    }
}
