package BL;

import BL.Domain.Message;

public interface MessageFormatter {
    Message formatMessage(String content);
}
