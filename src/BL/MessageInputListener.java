package BL;

import BL.Domain.Message;

public interface MessageInputListener {
    void receive(Message message);
}
