package BL;

import BL.Domain.Message;
import BL.Domain.RailroadMessage;
import BL.Domain.RideMessage;
import BL.Domain.SignalMessage;


public class MessageFormatAdapter implements MessageFormatter {
    private CastorFormatter castorFormatter;

    public MessageFormatAdapter() {
        castorFormatter= new CastorFormatter();
        System.out.println("Test git");
    }

    @Override
    public Message formatMessage(String content) {
        if (content.startsWith("<?xml"))  {
            return castorFormatter.formatMessage(content);
        } else

            if (content.contains("Ride")) {
                return new RideMessage(content, content, content, content);
            } else if (content.contains("Signal")) {
                return new SignalMessage(content, true);
            } else {
                return new Message();
            }

    }
}
