package BL.Domain;

public class SpeedMessage extends RailroadMessage{
    private String sectionId;
    private double speedMS;

    public SpeedMessage(String rideId, String sectionId, double speedMS) {
        super(rideId);
        this.sectionId = sectionId;
        this.speedMS = speedMS;
    }

    public String getSectionId() {
        return sectionId;
    }

    public void setSectionId(String sectionId) {
        this.sectionId = sectionId;
    }

    public double getSpeedMS() {
        return speedMS;
    }

    public void setSpeedMS(double speedMS) {
        this.speedMS = speedMS;
    }

    @Override
    public String toString() {
        return "SpeedMessage{" +
                "sectionId='" + sectionId + '\'' +
                ", speedMS=" + speedMS +
                '}';
    }
}
