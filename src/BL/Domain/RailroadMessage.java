package BL.Domain;

public class RailroadMessage extends Message {

    private final String rideId;

    public RailroadMessage(String rideId) {
        this.rideId = rideId;
    }

    public String getRideId() {
        return rideId;
    }

    @Override
    public String toString() {
        return "RailroadMessage{" +
                "rideId='" + rideId + '\'' +
                '}';
    }
}
