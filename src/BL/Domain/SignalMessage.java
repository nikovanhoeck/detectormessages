package BL.Domain;

public class SignalMessage extends Message{

    private final String railwayBarrierId;
    private final boolean status;

    public SignalMessage(String railwayBarrierId, boolean status) {
        this.railwayBarrierId = railwayBarrierId;
        this.status = status;
    }

    public String getRailwayBarrierId() {
        return railwayBarrierId;
    }

    @Override
    public String toString() {
        return "SignalMessage{" +
                "railwayBarrierId='" + railwayBarrierId + '\'' +
                ", status=" + status +
                '}';
    }
}
