package BL;

import BL.Domain.Message;
import org.exolab.castor.xml.MarshalException;
import org.exolab.castor.xml.Unmarshaller;
import org.exolab.castor.xml.ValidationException;

import java.io.StringReader;
import java.io.StringWriter;

public class CastorFormatter implements MessageFormatter {
    @Override
    public Message formatMessage(String content) {
        Message msg = new Message();
        StringReader reader = new StringReader(content);
        try {
            //System.out.println("Content: " + reader.toString());
            msg = (Message) Unmarshaller.unmarshal(Message.class,reader);
        } catch (MarshalException | ValidationException e) {
            e.printStackTrace();
        }
        return msg;
    }
}
