package BL;

import org.junit.Test;

import java.util.Scanner;

import static org.junit.Assert.*;

public class MessageTest {


    public static void main(String[] args) {
        String uri = "amqp://eitmyywb:aJi1NXbiDYQcTMC_TJnvmnQz3cBLjxbA@impala.rmq.cloudamqp.com/eitmyywb";

        MessageFormatter formatter = new MessageFormatAdapter();
        MessageInputService inputservice = new RabbitMQ(uri, "TestTrainMessages", formatter);
        inputservice.initialize(new CollideService());

        Scanner scanner = new Scanner(System.in);
        boolean shutdown = false;
        do{
            String word = scanner.nextLine();
            if (word.equals("exit")) {
                shutdown = true;
                inputservice.shutdown();
            }
        } while (shutdown);

    }
}